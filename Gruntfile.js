/*jslint node: true */
'use strict';
module.exports = function(grunt) {

    // Load all taks
    require('load-grunt-tasks')(grunt);


    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        assetsFolder: '<%= pkg.assetsFolder %>',
        sass: {
            dev: {
                options: {
                    style: 'expanded'
                },
                files: {
                   'assets/main.css': 'assets/_scss/main.scss' 
                }
            },
            build: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'assets/main.css': 'assets/_scss/main.scss'
                }
            }
        },
        jshint: {
            options: {
                browser: true,
                jquery: true,
                undef: true,
                unused: true,
                devel: true,
                globals: {
                    module: false,
                    Modernizr: false,
                    define: false,
                    require: false
                }
            },
            dev: {
                files: {
                    src: ['Gruntfile.js', 'assets/_js/**/*.js']
                }
            },
            build: {
                files: {
                    src: ['Gruntfile.js', 'assets/_js/**/*.js']
                }
            }

        },
        modernizr: {

            "devFile" : "assets/_bower_components/modernizr/modernizr.js",
            "outputFile" : "_build/assets/modernizr.js",

            // Based on default settings on http://modernizr.com/download/
            "extra" : {
                "shiv" : true,
                "printshiv" : false,
                "load" : true,
                "mq" : false,
                "cssclasses" : true
            },

            // Based on default settings on http://modernizr.com/download/
            "extensibility" : {
                "addtest" : false,
                "prefixed" : false,
                "teststyles" : false,
                "testprops" : false,
                "testallprops" : false,
                "hasevents" : false,
                "prefixes" : false,
                "domprefixes" : false
            },

            // By default, source is uglified before saving
            "uglify" : true,

            // By default, this task will crawl your project for references to Modernizr tests.
            // Set to false to disable.
            "parseFiles" : true,

            // When parseFiles = true, matchCommunityTests = true will attempt to
            // match user-contributed tests.
            "matchCommunityTests" : false,
        },
        jekyll: {
            options: {
                bundleExec: false,
                src : './',
            },
            dev: {
                options: {
                    config: '_config.yml,_config.dev.yml'
                }
            },
            build: {
                options: {
                    config: '_config.yml,_config.build.yml'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 8', ]
            },
            no_dest: {
                src: 'assets/main.css'
            }
        },
        watch: {
            scss: {
                files: [ '<%= assetsFolder %>/_scss/*.scss', '<%= assetsFolder %>/_scss/**/*.scss' ],
                tasks: [ 'sass:dev', 'autoprefixer', 'jekyll:dev', 'requirejs:dev' ]
            },
            js: {
                files: [ '<%= assetsFolder %>/_js/*.js', '<%= assetsFolder %>/_js/**/*.js' ],
                tasks: [ 'jshint:dev', 'jekyll:dev', 'requirejs:dev' ]
            },
            html: {
                files: [ '*.html', '**/*.html', '!_dev/*', '!_build/*' ],
                tasks: [ 'jekyll:dev', 'requirejs:dev' ]
            },
            config: {
                files: [ '*.yml' ],
                tasks: [ 'jekyll:dev', 'requirejs:dev' ]
            }
        },
        browser_sync: {
            files: {
                src: [ 
                    '_dev/assets/main.css',
                    '_dev/*.html',
                    '_dev/**/*.html'
                ]
            },
            options: {
                watchTask: true,
                server: {
                    baseDir: '_dev'
                },
                ports: {
                    min: 4000
                },
                open: false
            }
        },
        requirejs: {
            options: {
                mainConfigFile: '<%= assetsFolder %>/_js/config.js',
                removeCombined: true
            },
            dev: {
                options: {
                    optimize: 'none',
                    dir: '_dev/assets/js/'
                }
            },
            build: {
                options: {
                    optimize: 'uglify',
                    dir: '_build/assets/js/'
                }
            }
        },
        bower: {
            all: {
                rjsConfig: '<%= assetsFolder %>/_js/config.js',
                options: {
                    exclude: [ 'modernizr', 'normalize-scss' ]
                }
            }
        }

    });


    grunt.registerTask('dev', [
        'sass:dev',
        'jshint:dev',
        'autoprefixer',
        'jekyll:dev',
        'bower:all',
        'requirejs:dev',
        'browser_sync',
        'watch'
    ]);

    grunt.registerTask('build', [
        'sass:build',
        'jshint:build',
        'autoprefixer',
        'jekyll:build',
        'requirejs:build',
        'modernizr'
    ]);
};
